var nodeSwitcher = true;

function getNodes(switcher) {
  if(switcher) {
    var inNodes = {
        "name": "Andrew Brave",
        "color": "rgba(255, 255, 255, 0.5)",
        "level": "7.3",
        "dept": "NYC",
        "children": [
            {
                "name": "Spirit",
                "color": "rgba(255, 0, 0, 0.5)",
                "level": "6",
                "size": 23,
                "chidlren": [
                    {
                        "name": "HTML 5",
                        "color": "rgba(255, 255, 255, 0.5)",
                        "level": "7",
                        "size": 23
                    },
    				{
                        "name": "HTML 4",
                        "color": "rgba(255, 255, 255, 0.5)",
                        "level": "9",
                        "size": 23
                    }
                ],
            },
            {
                "name": "Mind",
                "color": "rgba(0, 0, 255, 0.5)",
                "level": "9",
                "size": 23,
    			"chidlren": [
                    {
                        "name": "CSS 2",
                        "color": "rgba(255, 255, 255, 0.5)",
                        "level": "9",
                        "size": 23
                    },
    				{
                        "name": "CSS 3",
                        "color": "rgba(255, 255, 255, 0.5)",
                        "level": "7",
                        "size": 23
                    }
                ],
            },
            {
                "name": "Body",
                "color": "rgba(0, 255, 0, 0.5)",
                "level": "7",
                "size": 23
            }
        ],
        "size": 30
    };
    return inNodes;
  }
  else {
    var inNodes = {
        "name": "Andrew Brave",
        "color": "rgba(255, 255, 255, 0.5)",
        "level": "7.3",
        "dept": "NYC",
        "children": [
            {
                "name": "Music",
                "color": "rgba(102, 0, 51, 0.5)",
                "level": "6",
                "size": 23,
                "chidlren": [
                    {
                        "name": "HTML 5",
                        "color": "rgba(255, 255, 255, 0.5)",
                        "level": "7",
                        "size": 13
                    },
    				{
                        "name": "HTML 4",
                        "color": "rgba(255, 255, 255, 0.5)",
                        "level": "9",
                        "size": 23
                    }
                ],
            },
            {
                "name": "Data Science",
                "color": "rgba(0, 153, 153, 0.5)",
                "level": "9",
                "size": 23,
    			"chidlren": [
                    {
                        "name": "CSS 2",
                        "color": "rgba(255, 255, 255, 0.5)",
                        "level": "9",
                        "size": 23
                    },
    				{
                        "name": "CSS 3",
                        "color": "rgba(255, 255, 255, 0.5)",
                        "level": "7",
                        "size": 23
                    }
                ],
            },
            {
                "name": "Sport",
                "color": "rgba(255, 255, 0, 0.5)",
                "level": "7",
                "size": 23
            }
        ],
        "size": 30
    };
    return inNodes;
  }
}

  getGraph();

  function getGraph() {
    var w = 600,
        h = 400,
        z = d3.scale.category10();

    var force = d3.layout.force()
        .linkDistance(150)
        .size([w, h])
    	.charge(-630);

    var svg = d3.select("#chart")
    	.append('svg')
        .attr("width", w)
        .attr("height", h)
    	.style("background", "rgb(255, 255, 255, 0)");


    var nodes = flatten(getNodes(nodeSwitcher));
    var links = d3.layout.tree().links(nodes);

    force
        .nodes(nodes)
        .links(links)
        .friction(0.8)
        .start();

    //link settings
    var link = svg.selectAll("line")
        .data(links)
        .enter().insert("line")
        .style("stroke", "rgba(255, 255, 255, 0)")
        .style("stroke-width", "1px");

    var node = svg.selectAll(".generalClass")
        .data(nodes)
  	  .enter().append("a")
      .attr("xlink:href", function(d) {
          if (d.name == "Music")
            return "music"
      })
      .append("circle")
      .attr("r", function(d){return d.size*2})
      .attr("class", 'generalClass')
      .style("fill", function(d) { return d.color })
      .style("stroke", "#000")
  	  // .call(d3.helper.tooltip()
      //             .attr({class: function(d, i) { return d + ' ' +  i + ' A'; }})
      //             .style({color: 'black'})
      //             .text(function(d, i){ return ''+d.level+ '/10'; })
      //         )
      .call(force.drag)
      .on("click", function(d) {
          if (d.name == "Andrew Brave")
            updateNodes();

      });

      var text = svg.selectAll("text.node")
          .data(nodes)
    	  .enter().append("text")
          .text(function(d) {return d.name;})
          .attr("text-anchor","middle");


      force.on("tick", function(e) {
        link.attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

        node.attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });

        text.attr("dx", function (d) {return d.x; })
            .attr("dy", function (d) {return d.y + 4; });
      });

    function flatten(root) {
        var nodes = [];
        function recurse(node, depth) {
            if (node.children) {
                node.children.forEach(function (child) {
                    child.parent = node;
                    recurse(child, depth);
                });
            }
            node.depth = depth;
            nodes.push(node);
        }
        recurse(root);
        return nodes;
    }
  }

  function updateNodes() {
    nodeSwitcher = !nodeSwitcher;
    d3.select("svg").remove();
    getGraph();
  }
