
var w = 960,
    h = 570,
    x = d3.scale.linear().range([0,w]),
    y = d3.scale.linear().range([0,h]),
    headerHeight = 20,
    color = d3.scale.category20c(),
    root,
    node;

var treemap = d3.layout.treemap()
    .round(false)
    .size([w, h])
    .sticky(true)
    .value(function(d) {return d.timetoimplement; });


var svg = d3.select("#body").append("div")
    .attr("class", "chart")
    .style("width", w + "px")
    .style("height", h + "px")
  .append("svg:svg")
    .attr("width", w)
    .attr("height", h)
  .append("svg:g")
    .attr("transform", "translate(.5,.5)");

d3.json("tools.json", function(data) {
  node = root = data;


  var parents = treemap.nodes(root)
    .filter(function(d) {
          return d.children;
    });


  //console.log(data);
  var nodes = treemap.nodes(root)
      .filter(function(d) {
            // console.log("d.children", d.children);
            // console.log("!d.children", !d.children);
            return !d.children;
          });


  var ChildrenCell = svg.selectAll("g.cell.child")
      .data(nodes)
    .enter().append("svg:g")
      .attr("class", "cell child")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
    //   .on("mouseover", showTooltip)
      .on("click", function(d) { return zoom(node == d.parent ? root : d.parent); });


  var ParentCell = svg.selectAll("g.cell.parent")
      .data(parents)
    .enter().append("svg:g")
      .attr("class", "cell parent")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
    //   .on("mouseover", showTooltip)
    // .on("click", function(d) { return zoom(d); });


  ChildrenCell.append("svg:rect")
      .attr("width", function(d) { return d.dx - 1; })
      .attr("height", function(d) { return d.dy - 1; })
      .style("fill", function(d) {return color(d.parent.name); });

  ParentCell.append("svg:rect")
    .attr("width", function(d) { return d.dx - 1; })
    .attr("height", headerHeight)
    // .style("fill", rgb(26,60,104));

  ChildrenCell.append("svg:text")
      .attr("x", function(d) { return d.dx / 2; })
      .attr("y", function(d) { return d.dy / 2; })
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .text(function(d) { return d.name; })
      .style("opacity", function(d) { d.w = this.getComputedTextLength(); return d.dx > d.w ? 1 : 0; });


  ParentCell.append("svg:text")
      .attr("x", function(d) { return d.dx / 2; })
      .attr("y", function(d) { return (d.dy - (d.dy-headerHeight))/ 2; })
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .text(function(d) { return d.name; })
      .style("opacity", function(d) { d.w = this.getComputedTextLength(); return d.dx > d.w ? 1 : 0; });

// Esto es para mostrar otra inforamción bajo la que ya se tiene en cada cell
  // text.append("svg:tspan")
  //   .attr("x", function(d) { return d.dx / 2; })
  //   .attr("y", function(d) { return d.dy / (2-0.7); })
  //   .attr("dy", ".35em")
  //   .attr("text-anchor", "middle")
  //   .text(function(d) { return d.timetoimplement; })
  //   .style("opacity", function(d) { d.w = this.getComputedTextLength(); return d.dx > d.w ? 1 : 0; });


/// Lo que agruegué de último

  var title = ChildrenCell.append("svg:title")
      .text(function(d) {
          if(d3.select("#selectOptions").node().value == "SumByTimeToImplement")
            return "value: " + d.timetoimplement + "%";
          if(d3.select("#selectOptions").node().value == "Comments")
            return "comments: " + d.extracomments;
          if(d3.select("#selectOptions").node().value == "SumByBigDataSupport")
            return "value: " + d.bigdatasupport + "%";
            });



  // var info = d3.select(".chart").append("div").attr("class","info");
  //    .
  //
  // function showTooltip(d) {
  //     info.style("display", "block")
  //         .text("Hello World and this is d: ", d);
  // }
  //
  // function hideTooltip() {
  //     info.style("display", "none");
  // }
  /// Lo que agruegué de último


  d3.select(window).on("click", function() { zoom(root); });

  d3.select("select").on("change", function() {
    //treemap.value(this.value == "size" ? size : count).nodes(root);
    title.text(function(d) {
        if(d3.select("#selectOptions").node().value == "SumByTimeToImplement")
          return "value: " + d.timetoimplement + "%";
        if(d3.select("#selectOptions").node().value == "Comments")
          return "comments: " + d.extracomments;
        if(d3.select("#selectOptions").node().value == "SumByBigDataSupport")
          return "value: " + d.bigdatasupport + "%";
          });

    treemap.value((this.value == "SumByTimeToImplement") ? timetoimplement : (this.value == "SumByBigDataSupport") ? bigdatasupport : extracomments_value).nodes(root);
    /*treemap.value(
      function() {
        if (this.value == "SumByTimeToImplement") {
          timetoimplement
        }
        if (this.value == "SumByBigDataSupport") {
          bigdatasupport
        }
        if (this.value == "Comments") {
          extracomments_value
        }
      }
    ).nodes(root);*/
    zoom(node);
  });

    function timetoimplement(d) {
      return d.timetoimplement;
    }

    function bigdatasupport(d) {
      return d.bigdatasupport;
    }

    function extracomments_value(d) {
      return d.extracomments_value;
    }

    function zoom(d) {
      var kx = w / d.dx, ky = h / d.dy;
      x.domain([d.x, d.x + d.dx]);
      y.domain([d.y, d.y + d.dy]);

      var t = svg.selectAll("g.cell.child").transition()
          .duration(d3.event.altKey ? 7500 : 750)
          .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

      t.select("rect")
          .attr("width", function(d) { return kx * d.dx - 1; })
          .attr("height", function(d) { return ky * d.dy - 1; })

      t.select("text")
          .attr("x", function(d) { return kx * d.dx / 2; })
          .attr("y", function(d) { return ky * d.dy / 2; })
          .style("opacity", function(d) { return kx * d.dx > d.w ? 1 : 0; });


      var tP = svg.selectAll("g.cell.parent").transition()
          .duration(d3.event.altKey ? 7500 : 750)
          .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

      tP.select("rect")
          .attr("width", function(d) { return kx * d.dx; })
          .attr("height", headerHeight);

      tP.select("text")
          .attr("x", function(d) { return  kx * d.dx / 2; })
          .attr("y", function(d) { return (h - (h-headerHeight)) / 2; })
          .style("opacity", function(d) { return kx * d.dx > d.w ? 1 : 0; });


      node = d;
      d3.event.stopPropagation();
    }


});
