//working well wothout titles and with large text problem
var w = 960,
    h = 570,
    x = d3.scale.linear().range([0,w]),
    y = d3.scale.linear().range([0,h]),
    color = d3.scale.category20c(),
    root,
    node;

var treemap = d3.layout.treemap()
    .round(false)
    .size([w, h])
    .sticky(true)
    .value(function(d) {return d.timetoimplement; });


var svg = d3.select("#body").append("div")
    .attr("class", "chart")
    .style("width", w + "px")
    .style("height", h + "px")
  .append("svg:svg")
    .attr("width", w)
    .attr("height", h)
  .append("svg:g")
    .attr("transform", "translate(.5,.5)");

d3.json("tools.json", function(data) {
  node = root = data;

  //console.log(data);
  var nodes = treemap.nodes(root)
      .filter(function(d) {
            // console.log("d.children", d.children);
            // console.log("!d.children", !d.children);
            return !d.children;
          });

  var parents = treemap.nodes(root)
      .filter(function(d) {
            return d.children;
      });

  var cell = svg.selectAll("g")
      .data(nodes)
    .enter().append("svg:g")
      .attr("class", "cell")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
    //   .on("mouseover", showTooltip)
      .on("click", function(d) { return zoom(node == d.parent ? root : d.parent); });
    //   .on("mouseover", function(d) {
    //       // this variable will be used in a loop to store the current node being inspected
    //       var currentNode = d;
    //       // this array will hold the names of each subsequent parent node
    //       var nameList = [currentNode.name];
    //       // as long as the current node has a parent...
    //       while (typeof currentNode.parent === 'object') {
    //         // go up a level in the hierarchy
    //         currentNode = currentNode.parent;
    //         // add the name to the beginning of the list
    //         nameList.unshift(currentNode.name);
    //       }
    //       // now nameList should look like ['flare','animate','interpolate']
    //       //  join the array with slashes (as you have in your example)
    //       nameList = nameList.join('/');
    //       // now nameList should look like 'flare/animate/interpolate'
    //       //  use this to set the tooltip text
    //       $('#tooltip').text('Mouse hovering ' + nameList + '. Cell size = ' + d.area);
    //   });



  cell.append("svg:rect")
      .attr("width", function(d) { return d.dx - 1; })
      .attr("height", function(d) { return d.dy - 1; })
      .style("fill", function(d) {return color(d.parent.name); });

  cell.append("svg:text")
      .attr("x", function(d) { return d.dx / 2; })
      .attr("y", function(d) { return d.dy / 2; })
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .text(function(d) { return d.name; })
      .style("opacity", function(d) { d.w = this.getComputedTextLength(); return d.dx > d.w ? 1 : 0; });

// Esto es para mostrar otra inforamción bajo la que ya se tiene en cada cell
  // text.append("svg:tspan")
  //   .attr("x", function(d) { return d.dx / 2; })
  //   .attr("y", function(d) { return d.dy / (2-0.7); })
  //   .attr("dy", ".35em")
  //   .attr("text-anchor", "middle")
  //   .text(function(d) { return d.timetoimplement; })
  //   .style("opacity", function(d) { d.w = this.getComputedTextLength(); return d.dx > d.w ? 1 : 0; });


/// Lo que agruegué de último

  var title = cell.append("svg:title")
      .text(function(d) {
          if(d3.select("#selectOptions").node().value == "SumByTimeToImplement")
            return d.timetoimplement;
          else
            return d.bigdatasupport;
            });



  // var info = d3.select(".chart").append("div").attr("class","info");
  //    .
  //
  // function showTooltip(d) {
  //     info.style("display", "block")
  //         .text("Hello World and this is d: ", d);
  // }
  //
  // function hideTooltip() {
  //     info.style("display", "none");
  // }
  /// Lo que agruegué de último


  d3.select(window).on("click", function() { zoom(root); });

  d3.select("select").on("change", function() {
    //treemap.value(this.value == "size" ? size : count).nodes(root);
    title.text(function(d) {
        if(d3.select("#selectOptions").node().value == "SumByTimeToImplement")
          return d.timetoimplement;
        else
          return d.bigdatasupport;
          });

    treemap.value((this.value == "SumByTimeToImplement") ? timetoimplement : bigdatasupport).nodes(root);
    zoom(node);
  });

    function timetoimplement(d) {
      return d.timetoimplement;
    }

    function bigdatasupport(d) {
      return d.bigdatasupport;
    }

    function zoom(d) {
      var kx = w / d.dx, ky = h / d.dy;
      x.domain([d.x, d.x + d.dx]);
      y.domain([d.y, d.y + d.dy]);

      var t = svg.selectAll("g.cell").transition()
          .duration(d3.event.altKey ? 7500 : 750)
          .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

      t.select("rect")
          .attr("width", function(d) { return kx * d.dx - 1; })
          .attr("height", function(d) { return ky * d.dy - 1; })

      t.select("text")
          .attr("x", function(d) { return kx * d.dx / 2; })
          .attr("y", function(d) { return ky * d.dy / 2; })
          .style("opacity", function(d) { return kx * d.dx > d.w ? 1 : 0; });

      node = d;
      d3.event.stopPropagation();
    }


});
