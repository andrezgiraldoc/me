//alert("hello my friend!");
// d3.selectAll("p").style("color", function(){
//     return "hsl(" + Math.random()*360 + ", 71%, 50%)";
// });

// d3.select("body")
//   .selectAll("p")
//   .data([120,40,80,100])
//   .enter().append()
//   .text(function(d) { return "I'm number " + d + "!"; })
//   .exit().remove();


////////////////////////////////////////////////////////////////////////////////
var svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

var fader = function(color) { return d3.interpolateRgb(color, "#fff")(0.2); },
    color = d3.scaleOrdinal(d3.schemeCategory20.map(fader)),
    format = d3.format(",d");

var treemap = d3.treemap()
    .tile(d3.treemapResquarify)
    .size([width, height])
    .round(true)
    .paddingInner(1);


d3.json("tools.json", function(error, data) {
  if (error) throw error;

  var root = d3.hierarchy(data)
            .eachBefore(function(d){
                d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name;
            })
            .sum(SumByTimeToImplement);
            // .sort(function(a, b) {
            //     return b.height - a.height || b.value - a.value;
            // });

  treemap(root);

  var cell = svg.selectAll("g")
    .data(root.leaves())
    .enter().append("g")
      .attr("transform", function(d) { return "translate(" + d.x0 + "," + d.y0 + ")"; });

  cell.append("rect")
      .attr("id", function(d) { return d.data.id; })
      .attr("width", function(d) { return d.x1 - d.x0; })
      .attr("height", function(d) { return d.y1 - d.y0; })
      .attr("fill", function(d) { return color(d.parent.data.id); });

  cell.append("clipPath")
      .attr("id", function(d) { return "clip-" + d.data.id; })
      .append("use")
      .attr("xlink:href", function(d) { return "#" + d.data.id; });

  cell.append("text")
      .attr("clip-path", function(d) { return "url(#clip-" + d.data.id + ")"; })
    .selectAll("tspan")
      .data(function(d) { return d.data.name.split(/(?=[A-Z][^A-Z])/g); })
    .enter().append("tspan")
      .attr("x", 4)
      .attr("y", function(d, i) { return 13 + i * 10; })
      .text(function(d) { return d; });

  cell.append("title")
      .text(function(d) { return d.data.id + "\n" + format(d.value); });

    d3.selectAll("input")
      .data([SumByTimeToImplement, SumByBigDataSupport], function(d) { return d ? d.name : this.value; })
      .on("change", changed);

    var timeout = d3.timeout(function() {
    d3.select("input[value=\"SumByBigDataSupport\"]")
        .property("checked", true)
        .dispatch("change");
    }, 2000);

    function changed(sum) {
        console.log("I changed");
        timeout.stop();
        treemap(root.sum(sum));
        cell.transition()
            .duration(750)
            .attr("transform", function(d) { return "translate(" + d.x0 + "," + d.y0 + ")"; })
          .select("rect")
            .attr("width", function(d) { return d.x1 - d.x0; })
            .attr("height", function(d) { return d.y1 - d.y0; });
    }

    function SumByTimeToImplement(d) {
        // console.log("hello d", d)
        console.log("hello d of Time2Implement: ", d.timetoimplement)
        return d.timetoimplement;
    }

    function SumByBigDataSupport(d) {
        // console.log("hello d", d)
        console.log("hello d of Big Data: ", d.bigdatasupport)
        return d.bigdatasupport;
    }

});
////////////////////////////////////////////////////////////////////////////////


// var chartWidth = 600;
// var chartHeight = 600;
// var xscale = d3.scaleLinear().range([0, chartWidth]);
// var yscale = d3.scaleLinear().range([0, chartHeight]);
// var color = d3.schemeCategory10;
// var headerHeight = 20;
// var headerColor = "#555555";
// var transitionDuration = 500;
// var root;
// var node;
//
// var treemap = d3.treemap()
//    .round(false)
//    .size([chartWidth, chartHeight])
//    // .sticky(true)
//    .value(function(d){
//       return d.timetoimplement;
//   });
//
// var chart = d3.select("body")
//     .append("svg:svg")
//     .attr("width", chartWidth)
//     .attr("height", chartHeight)
//     .append("svg:g");
//
// d3.json("tools.json", function(data){
//      node = root = data;
//      var nodes = treemap.nodes(root);
// });
