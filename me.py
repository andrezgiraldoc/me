from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/music')
def music():
    return render_template('music.html')


@app.route('/music_gallery')
def music_gallery():
    return render_template('music-gallery.html')


@app.route('/music_blog')
def music_blog():
    return render_template('music-blog-list.html')


@app.route('/music_blog_post_1')
def music_blog_post_1():
    return render_template('music-blog-post-1.html')


@app.route('/music_personal')
def music_personal():
    return render_template('music-personal.html')


if __name__ == '__main__':
    app.run(debug=True)
